import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from '../../model/item';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { FormBuilder } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { getCounter, getItems, getItemsErrors } from './store/items.selectors';
import { addItem, addItemFailed, addItemSuccess, deleteItem, increment, initPage } from './store/items.actions';
import { getHttpStatus, HttpStatusState } from '../../core/store/http-status.reducer';
import { selectUrl } from '../../core/store/router.selectors';
import { go } from '../../core/store/router.actions';

@Component({
  selector: 'app-items',
  template: `

    {{routerUrl$ | async}}
    <hr>

    <pre>{{httpStatus$ | async | json}}</pre>
    <div style="background-color: red" *ngIf="(httpStatus$ | async) as status">
      <div *ngIf="status.hasError && status.actionType === addItemFailedType">
        errore in inserimento
      </div>
    </div>
    <div style="background-color: red" *ngIf="itemsErrors$ | async">
      Ahia!
    </div>
    <!--No items-->
    <ng-template #noItems>
      <div>no products found</div>
    </ng-template>

    <!--item form-->
    <form [formGroup]="form" (submit)="addItemHandler()">
      <input type="text" formControlName="name">
      <button type="submit">ADD</button>
    </form>

    <!--<button (click)="loadItemsHandler()">load items</button>-->
    <button (click)="incHandler()">+ {{(counter$ | async)}}</button>

    <!--item list-->
    <ng-container *ngIf="(items$ | async) as items">
      <div *ngIf="items.length else noItems">
        total: {{items?.length}} items;
        <hr>
        <li *ngFor="let item of items">
          {{item.name}}
          <button (click)="deleteHandler(item.id)">Delete</button>
        </li>
      </div>

    </ng-container>
  `,
  styles: [
  ]
})
export class ItemsComponent {
  items$: Observable<Item[]> = this.store.select(getItems)
  itemsErrors$: Observable<boolean> = this.store.select(getItemsErrors)
  counter$: Observable<number> = this.store.select(getCounter)
  httpStatus$: Observable<HttpStatusState> = this.store.select(getHttpStatus)
  routerUrl$: Observable<string> = this.store.select(selectUrl)
  addItemFailedType = addItemFailed.type;

  form = this.fb.group({
    name: 'pluto'
  })

  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder,
    private actions: Actions
  ) {
   /* setTimeout(() => {
      this.store.dispatch(go({ path: 'pexels'}))
    }, 2000)*/
    this.actions
      .pipe(
        // filter(action => action.type === addItemSuccess.type)
        ofType(addItemSuccess),
      )
      .subscribe(action => {
        console.log(action.item)
        this.form.reset();
      })

    this.store.dispatch(initPage())
  }

  deleteHandler(id: number) {
    this.store.dispatch(deleteItem({ id: id }))
  }

  addItemHandler() {
    this.store.dispatch(addItem({ item: { ...this.form.value } }))

  }

  loadItemsHandler() {
    this.store.dispatch(initPage())
    /*this.store.dispatch(loadItems())
    this.store.dispatch(loadCounter())*/
  }

  incHandler() {
    this.store.dispatch(increment())
  }

}
