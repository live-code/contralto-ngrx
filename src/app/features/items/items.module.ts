import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { ActionReducerMap, StoreFeatureModule, StoreModule } from '@ngrx/store';
import { itemsReducer, ItemsState } from './store/items.reducer';
import { counterReducer, CounterState } from './store/counter.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';

export interface ItemsFeatureState {
   items: ItemsState;
   counter: CounterState;
}

const reducers: ActionReducerMap<ItemsFeatureState> = {
  items: itemsReducer,
  counter: counterReducer,
}
@NgModule({
  declarations: [
    ItemsComponent
  ],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('itemsFeature', reducers ),
    EffectsModule.forFeature([ ItemsEffects ])
  ]
})
export class ItemsModule { }
