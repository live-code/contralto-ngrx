import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import {
  addItem, addItemFailed,
  addItemSuccess,
  deleteItem, deleteItemFailed,
  deleteItemSuccess, increment, incrementSuccess, initPage, loadCounter, loadCounterSuccess,
  loadItems,
  loadItemsFailed,
  loadItemsSuccess
} from './items.actions';
import { catchError, concatMap, delay, exhaustMap, map, mergeMap, of, switchMap, tap, withLatestFrom } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { CounterState } from './counter.reducer';
import { Item } from '../../../model/item';
import { AppState } from '../../../app.module';
import { getCounter } from './items.selectors';

@Injectable()
export class ItemsEffects {
  initPage$ = createEffect(() => this.actions$.pipe(
    ofType(initPage),
    switchMap(
      () => [
        loadItems(),
        loadCounter()
      ]
    )
  ));

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    exhaustMap(
      () => this.http.get<Item[]>('http://localhost:3000/items')
        .pipe(
          delay(1000),
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFailed()))
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    concatMap(
      action => this.http.post<Item>('http://localhost:3000/items', action.item)
        .pipe(
          map(item => addItemSuccess({ item })),
          catchError(() => of(addItemFailed()))
        )
    )
  ))

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(
      ({ id }) => this.http.delete<void>(`http://localhost:3000/items/${id}`).pipe()
        .pipe(
          delay(1000),
          map(() => deleteItemSuccess({ id })),
          catchError(() => of(deleteItemFailed()))
        )
    )
  ))

  // counter effects
  counterItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadCounter),
    exhaustMap(
      () => this.http.get<CounterState>('http://localhost:3000/counter')
        .pipe(
          map(counter => loadCounterSuccess({ value: counter.value})),
          // catchError(() => of(loadItemsFailed()))
        )
    )
  ));
  counterIncrement$ = createEffect(() => this.actions$.pipe(
    ofType(increment),
    concatLatestFrom(() => this.store.select(getCounter)),
    mergeMap(
      ([action, counter]) => this.http.patch<CounterState>(`http://localhost:3000/counter`, { value: counter + 1})
        .pipe(
          delay(1000),
          map(({ value }) => incrementSuccess({ value })),
          // catchError
        )
    )
  ))


  constructor(
    private store: Store,
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
