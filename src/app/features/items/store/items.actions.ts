import { createAction, props } from '@ngrx/store';
import { Item } from '../../../model/item';


export const initPage = createAction(
  '[item page] init',
)

export const loadItems = createAction(
  '[items] load',
)
export const loadItemsSuccess = createAction(
  '[items] load success',
  props<{ items: Item[]}>()
)
export const loadItemsFailed = createAction(
  '[items] load failed',
)

export const addItem = createAction(
  '[items] add',
  props<{ item: Omit<Item, 'id'> }>()
)

export const addItemSuccess = createAction(
  '[items] add success',
  props<{ item: Item }>()
)
export const addItemFailed = createAction(
  '[items] add failed',
)


export const deleteItem = createAction(
  '[items] delete',
  props<{ id: number }>()
)

export const deleteItemSuccess = createAction(
  '[items] delete success',
  props<{ id: number }>()
)

export const deleteItemFailed = createAction(
  '[items] delete failed',
)


export const loadCounter = createAction(
  '[counter] load',
)

export const loadCounterSuccess = createAction(
  '[counter] load success',
  props<{ value: number}>()
)

export const increment = createAction(
  '[counter] increment',
)

export const incrementSuccess = createAction(
  '[counter] increment success',
  props<{ value: number}>()
)

