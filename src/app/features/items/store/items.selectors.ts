import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ItemsFeatureState } from '../items.module';

export const getItemFeature = createFeatureSelector<ItemsFeatureState>('itemsFeature')

export const getItems = createSelector(
  getItemFeature,
  state => state.items.list
)

export const getItemsErrors = createSelector(
  getItemFeature,
  state => state.items.hasError
)


export const getCounter = createSelector(
  getItemFeature,
  state => state.counter.value
)

