import { createReducer, on } from '@ngrx/store';
import { increment, incrementSuccess, loadCounterSuccess } from './items.actions';

export interface CounterState {
  value: number;
}

export const initialState:CounterState = { value: 0};

export const counterReducer = createReducer(
  initialState,
  on(loadCounterSuccess, (state, action) => ({ value: action.value})),
  on(incrementSuccess, (state, action) => ({ value: action.value}))
)
