import { createReducer, on } from '@ngrx/store';
import {
  addItem, addItemFailed,
  addItemSuccess,
  deleteItem, deleteItemFailed,
  deleteItemSuccess,
  loadItems,
  loadItemsFailed,
  loadItemsSuccess
} from './items.actions';
import { Item } from '../../../model/item';

export interface ItemsState {
  list: Item[];
  hasError: boolean;
  pending: boolean;
}
export const initialState: ItemsState = {
  list: [],
  hasError: false,
  pending: false
}

export const itemsReducer = createReducer(
  initialState,
  on(loadItems, state => ({...state, pending: true})),
  on(loadItemsFailed, (state) => ({ ...state, hasError: true, pending: false })),
  on(loadItemsSuccess, (state, action) => ({ list: [ ...action.items ], hasError: false, pending: false })),

  on(addItem, state => ({...state, pending: true})),
  on(addItemSuccess, (state, action) => ({ list: [ ...state.list, action.item], hasError: false, pending: false})),
  on(addItemFailed, (state) => ({ ...state, hasError: true, pending: false })),

  on(deleteItem, state => ({...state, pending: true})),
  on(deleteItemSuccess, (state, action) => ({ list: state.list.filter(item => item.id !== action.id), hasError: false, pending: false})),
  on(deleteItemFailed, (state) => ({ ...state, hasError: true, pending: false })),

)
