import { ActionReducer, ActionReducerMap, combineReducers } from '@ngrx/store';
import { PexelsVideoSearchState, searchReducer } from './search/pexels-video-search.reducer';
import { filtersReducer, PexelsFilterState } from './filters/filters.reducer';
import { PexelsPLayerState, playerReducer } from './player/player.reducer';


export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  filters: PexelsFilterState;
  player: PexelsPLayerState;
}

export const pexelsMainReducer: ActionReducer<PexelsVideoState> = combineReducers({
  search: searchReducer,
  filters: filtersReducer,
  player: playerReducer
})
