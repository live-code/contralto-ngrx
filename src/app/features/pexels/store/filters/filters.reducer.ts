import { createReducer, on } from '@ngrx/store';
import { filterByMinResolution } from './filters.action';

export interface PexelsFilterState {
  minResolution: number;
}

export const initialState: PexelsFilterState = {
  minResolution: 0,
};

export const filtersReducer = createReducer(
  initialState,
  on(filterByMinResolution, (state, action) => ({ minResolution: action.minResolution})),
);
