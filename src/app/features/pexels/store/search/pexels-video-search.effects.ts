import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { searchVideos, searchVideosFailed, searchVideosSuccess } from './pexels-video-search.actions';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { PexelsVideoResponse } from '../../model/pexels-video-response';
import { showVideo } from '../player/player.actions';

@Injectable()
export class PexelsVideoSearchEffects {
  search$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideos),
    switchMap(action => {
      return this.http.get<PexelsVideoResponse>(`https://api.pexels.com/videos/search?query=${action.text}&per_page=15&page=1`, {
        headers: {
          Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
        }
      })
        .pipe(
          switchMap(res => [
            searchVideosSuccess({ items: res.videos }),
            showVideo({ video: res.videos[0] })
          ]),
          catchError(() => of(searchVideosFailed()))
        )
    })
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
