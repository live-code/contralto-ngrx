import { Video } from '../../model/pexels-video-response';
import { createReducer, on } from '@ngrx/store';
import { searchVideos, searchVideosFailed, searchVideosSuccess } from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
  text: string;
}

export const initialState: PexelsVideoSearchState = {
  list: [],
  hasError: false,
  pending: false,
  text: 'beach',
}

export const searchReducer = createReducer(
  initialState,
  on(searchVideos, (state, action) => ({ text: action.text, hasError: false, pending: true, list: []})),
  on(searchVideosSuccess, (state, action) => ({ ...state, hasError: false, pending: false, list: action.items})),
  on(searchVideosFailed, (state, action) => ({ ...state, hasError: true, pending: false}))
)
