import { createFeatureSelector, createSelector } from '@ngrx/store';
import {  PexelsVideoStateMain } from '../../pexels.module';
import { getCurrentVideo, getCurrentVideoId } from '../player/player.selectors';

export const getPexelFeature = createFeatureSelector<PexelsVideoStateMain>('pexels-video')
/*

export const getPexelsVideos = createSelector(
  getPexelFeature,
  ((state) => state.search.list)
)
*/


export const getPexelsVideos = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.search.list.filter(item => item.width > state.pexelsPlayer.filters.minResolution))
)


export const getPexelsVideosSearchPending = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.search.pending)
)

export const getPexelsVideosSearchError = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.search.hasError)
)


export const getPexelsVideosSearchText = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.search.text)
)


export const getPexelsVideoPictures = createSelector(
  getPexelsVideos,
  getCurrentVideoId,
  (videos, currentVideoId) => videos.find(v => v.id === currentVideoId)?.video_pictures
    .map(imgObj => imgObj.picture)
)
