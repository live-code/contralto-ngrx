import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const searchVideos = createAction(
  '[pexel-video] search',
  props<{ text: string }>()
)

export const searchVideosSuccess = createAction(
  '[pexel-video] search success',
  props<{ items: Video[] }>()
)

export const searchVideosFailed = createAction(
  '[pexel-video] search failed',
)
