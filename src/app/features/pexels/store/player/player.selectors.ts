import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoStateMain } from '../../pexels.module';

export const getPexelFeature = createFeatureSelector<PexelsVideoStateMain>('pexels-video')

export const getCurrentVideo = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.player.currentVideo)
)

export const getCurrentVideoId = createSelector(
  getPexelFeature,
  ((state) => state.pexelsPlayer.player.currentVideo?.id)
)

export const getCurrentVideoUrl = createSelector(
  getPexelFeature,
  ((state) => {
    return state.pexelsPlayer.player.currentVideo?.video_files[0].link
  })
)
