import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const showVideo = createAction(
  '[pexel player] show video',
  props<{ video: Video }>()
)
