import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsRoutingModule } from './pexels-routing.module';
import { PexelsComponent } from './pexels.component';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { PexelsVideoSearchState, searchReducer } from './store/search/pexels-video-search.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { PexelsPLayerState, playerReducer } from './store/player/player.reducer';
import { filtersReducer, PexelsFilterState } from './store/filters/filters.reducer';
import { pexelsMainReducer, PexelsVideoState } from './store';

export interface PexelsVideoStateMain {
  pexelsPlayer: PexelsVideoState;
}

export const reducers: ActionReducerMap<PexelsVideoStateMain> = {
  pexelsPlayer: pexelsMainReducer
}

@NgModule({
  declarations: [
    PexelsComponent
  ],
  imports: [
    CommonModule,
    PexelsRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('pexels-video', reducers),
    EffectsModule.forFeature([PexelsVideoSearchEffects])
  ]
})
export class PexelsModule { }
