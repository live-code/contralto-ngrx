import { Component, OnInit } from '@angular/core';
import { Video } from './model/pexels-video-response';
import { debounceTime, distinctUntilChanged, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  getPexelsVideoPictures,
  getPexelsVideos,
  getPexelsVideosSearchPending, getPexelsVideosSearchText
} from './store/search/pexels-video-search.selectors';
import { FormControl } from '@angular/forms';
import { searchVideos, searchVideosSuccess } from './store/search/pexels-video-search.actions';
import { showVideo } from './store/player/player.actions';
import { getCurrentVideoUrl } from './store/player/player.selectors';
import { Actions, ofType } from '@ngrx/effects';
import { filterByMinResolution } from './store/filters/filters.action';

@Component({
  selector: 'app-pexels',
  template: `
    <input type="text" [formControl]="searchInput">
    <hr>
    
    <video 
      width="200" *ngIf="currentVideoUrl$ | async as url"
      controls
      [src]="url"
    >
      
    </video>
    <!--video pictures preview-->
    <div *ngIf="videoPictures$ | async as videoPictures">
      <img 
        *ngFor="let image of videoPictures"
        [src]="image" width="50"
      >
    </div>
    
    <hr>
    <select (change)="filterByResolution($event)">
      <option>0</option>
      <option>2000</option>
      <option>3000</option>
      <option>4000</option>
    </select>
    
    <!--videos search result-->
    <div *ngFor="let video of videos$ | async" (click)="showPreview(video)">
      <img [src]="video.image" width="100">
      {{video.width}} x {{video.height}}
    </div>
  `,
})
export class PexelsComponent implements OnInit {
  videos$: Observable<Video[]> = this.store.select(getPexelsVideos);
  videosSearchPending$: Observable<boolean> = this.store.select(getPexelsVideosSearchPending);
  videoPictures$: Observable<string[] | undefined> = this.store.select(getPexelsVideoPictures)
  currentVideoUrl$: Observable<string | undefined> = this.store.select(getCurrentVideoUrl)
  getPexelsVideosSearchText$: Observable<string> = this.store.select(getPexelsVideosSearchText)

  searchInput = new FormControl('')

  constructor(private store: Store, private actions$: Actions) {
    this.searchInput.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(text => {
        console.log('valueChanges', text)
        store.dispatch(searchVideos({ text }))
      });
  }

  // to fix: duplicate last action when store is imported from json
  ngOnInit(): void {
    this.getPexelsVideosSearchText$
      .subscribe(text => {
        console.log('ngOnInit', text)
        this.searchInput.setValue(text )
      })
  }

  showPreview(video: Video) {
    this.store.dispatch(showVideo({ video }))
  }

  filterByResolution(event: Event) {
    const minResolution = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution }));


  }
}
