import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/store/auth/auth.guard';

const routes: Routes = [
  { path: 'items', canActivate: [AuthGuard], loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'pexels', loadChildren: () => import('./features/pexels/pexels.module').then(m => m.PexelsModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
