import { Component } from '@angular/core';
import { logout } from './core/store/auth/auth.actions';
import { select, Store } from '@ngrx/store';
import { AppState } from './app.module';
import { getIsLogged } from './core/store/auth/auth.selectors';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-root',
  template: `

    <button routerLink="items">items</button>
    <button routerLink="pexels">pexels</button>
    <!--<button (click)="logoutHandler()" *ngIf="isLogged$ | async">Logout</button>-->
    <button (click)="logoutHandler()" *appIfLogged >Logout</button>
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
  // isLogged$: Observable<boolean> = this.store.pipe(select(getIsLogged));

  constructor(private store: Store<AppState>) { }

  logoutHandler(): void {
    this.store.dispatch(logout())
  }
}
