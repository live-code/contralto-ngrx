import { createFeatureSelector } from '@ngrx/store';
import { getSelectors, RouterReducerState } from '@ngrx/router-store';
import { AppState } from '../../app.module';

export const getRouter = createFeatureSelector<RouterReducerState>('router')
// export const getRouter = (state: AppState) => state.router

export const {
  selectUrl,
  selectRouteParams
} = getSelectors(getRouter)
