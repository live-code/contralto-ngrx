import { createReducer, on } from '@ngrx/store';
import {
  addItemFailed,
  addItemSuccess, deleteItemFailed,
  deleteItemSuccess,
  loadItemsFailed,
  loadItemsSuccess
} from '../../features/items/store/items.actions';
import { AppState } from '../../app.module';

export interface HttpStatusState {
  hasError: boolean,
  actionType: string;
}
const initialState: HttpStatusState = { hasError: false, actionType: ''};

export const httpStatusReducer = createReducer(
  initialState,
  on(addItemFailed, (state, action) => ({hasError: true, actionType: action.type})),
  on(deleteItemFailed, (state, action) => ({hasError: true, actionType: action.type})),
  on(loadItemsFailed, (state, action) => ({hasError: true, actionType: action.type})),
  on(loadItemsSuccess, (state, action) => ({hasError: false, actionType: action.type})),
  on(addItemSuccess, (state, action) => ({hasError: false, actionType: action.type})),
  on(deleteItemSuccess, (state, action) => ({hasError: false, actionType: action.type})),
)


// selectors

export const getHttpStatus = (state: AppState) => state.httpStatus;
