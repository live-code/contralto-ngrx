import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppState } from '../../../app.module';
import { getIsLogged } from './auth.selectors';
import { go } from '../router.actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(private store: Store<AppState>) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(getIsLogged),
      tap(isLogged => {
        if (!isLogged) {
          this.store.dispatch(go({ path: '/login' }));
        }
      })
    );
  }
}
