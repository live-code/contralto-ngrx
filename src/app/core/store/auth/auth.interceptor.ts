import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { catchError, Observable, throwError, withLatestFrom } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
import { getAuthToken, getIsLogged } from './auth.selectors';
import { AppState } from '../../../app.module';
import { go } from '../router.actions';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store<AppState>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getIsLogged).pipe(
      // Interceptor requires observable that complete
      // BETTER than first: avoid errors if there is no value,
      withLatestFrom(this.store.select(getAuthToken)),
      take(1),
      mergeMap(([isLogged, token]) => {
        const authReq = isLogged && !req.url.includes('pexels')? req.clone({
          setHeaders: { Authorization: 'Bearer ' + token },
        }) : req;
        return next.handle(authReq);
      }),
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          switch (err.status) {
            case 401:
              console.log('do something with 404 error', err);
              break;
            default:
            case 404:
              this.store.dispatch(go({ path: '/login'}));
              break;
          }
        }
        // return of(err);
        return throwError(err);
      }),
    );
  }
}
