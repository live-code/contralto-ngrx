import { createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';
import { Auth } from './auth';
import { syncWithLocalStorage } from './auth.effects';

export interface AuthState {
  auth: Auth | null;
  error: boolean;
}

export const initialState: AuthState = {
  auth: null,
  error: false
};

export const authReducer = createReducer(
  initialState,
  on(syncWithLocalStorage, (state, action) => ({auth: action.auth, error: false})),
  on(AuthActions.loginSuccess, (state, action) => ({auth: action.auth, error: false})),
  on(AuthActions.loginFailed, state => ({...state, error: true})),
  on(AuthActions.logout, () => ({auth: null, error: false})),
);
