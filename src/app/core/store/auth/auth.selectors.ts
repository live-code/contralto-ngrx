import { AppState } from '../../../app.module';

export const getIsLogged = (state: AppState) =>  !!state.authentication.auth;
export const getAuthToken = (state: AppState) =>  state.authentication.auth?.token;
export const getAuthDisplayName = (state: AppState) =>  state.authentication.auth?.displayName;
export const getAuthError = (state: AppState) =>  state.authentication.error;
