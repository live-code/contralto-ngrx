import { Injectable } from '@angular/core';
import { Actions, createEffect } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { debounceTime, fromEvent, map } from 'rxjs';
import { createAction, createReducer, on, props } from '@ngrx/store';

export interface UI {
  w: number, h: number
}
export const documentResize = createAction(
  '[resize]',
  props<UI>()
)

export const uiReducer = createReducer(
  {w: 0, h: 0},
  on(documentResize, (state, action) => ({ w: action.w, h: action.h}))
)


@Injectable()
export class UiEffects {

  resize$ = createEffect(() => fromEvent(window, 'resize').pipe(
    debounceTime(250),
    map(() => documentResize({ w: window.innerWidth, h: window.innerHeight}))
  ))

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {}
}
