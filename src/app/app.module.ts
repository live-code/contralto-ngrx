import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { httpStatusReducer, HttpStatusState } from './core/store/http-status.reducer';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/router.effects';
import { UI, UiEffects, uiReducer } from './core/store/ui.effects';
import { authReducer, AuthState } from './core/store/auth/auth.reducer';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { IfLoggedDirective } from './core/store/auth/if-logged.directive';


export interface AppState {
  httpStatus: HttpStatusState;
  router: RouterReducerState;
  ui: UI,
  authentication: AuthState
}

const reducers: ActionReducerMap<AppState> = {
  httpStatus: httpStatusReducer,
  router: routerReducer,
  ui: uiReducer,
  authentication: authReducer
}

@NgModule({
  declarations: [
    AppComponent,
    IfLoggedDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AuthEffects, RouterEffects, UiEffects ]),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    }),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    }),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
